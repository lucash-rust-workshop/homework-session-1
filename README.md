# Session 1
## How To run the solution
### Problem1
run `cargo run -- problem1 [path/to/inputFile]`

### Problem2
run `cargo run -- problem2 [path/to/inputFile]`

To use the alternative solution:  
run `cargo run -- problem2 -a [path/to/inputFile]`

### Homework Examples

to run the solution using the example given run the following commands:

  1. `cargo run -- problem1 problem1.txt`

  2.
    1. `cargo run -- problem2 problem2.txt`
    2. `cargo run -- problem2 -a problem2.txt`
