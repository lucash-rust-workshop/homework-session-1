use std::fs::read_to_string;
use std::path::PathBuf;

// To help with CLI interface and parsing the arguments
use structopt::StructOpt;

const BRAINFUCK_VALID_COMMANDS: [char; 8] = ['>', '<', '+', '-', '.', ',', '[', ']'];

#[derive(StructOpt)]
enum Session1 {
    #[structopt(name = "problem1")]
    Problem1 {
        /// Path to the input file
        #[structopt(parse(from_os_str))]
        path: PathBuf,
    },
    #[structopt(name = "problem2")]
    Problem2 {
        /// Use alternative function
        #[structopt(short = "a")]
        alternative: bool,
        /// Path to the input file
        #[structopt(parse(from_os_str))]
        path: PathBuf,
    },
}

fn main() {
    let args = Session1::from_args();

    match args {
        Session1::Problem1 { path } => problem_1(&path),
        Session1::Problem2 { path, alternative } => {
            if alternative {
                problem_2_alternate(&path)
            } else {
                problem_2(&path)
            }
        }
    }
}

fn problem_1(path: &PathBuf) {
    let content = read_from_file(&path);

    println!(
        "\nTotal: {}",
        content
            .lines()
            .filter_map(|l| l.parse::<i64>().ok())
            .fold(0, |sum, n| {
                println!("{}", n);
                sum + n
            })
    );
}

fn problem_2(path: &PathBuf) {
    let content = read_from_file(&path);

    for line in content.lines() {
        for c in line.chars() {
            match c {
                '>' | '<' | '+' | '-' | '.' | ',' | '[' | ']' => print!("{}", c),
                _ => print!(""),
            }
        }
    }
}

fn problem_2_alternate(path: &PathBuf) {
    let content = read_from_file(&path);

    println!(
        "{}",
        content
            .lines()
            .flat_map(|l| l.chars().filter(|c| BRAINFUCK_VALID_COMMANDS.contains(c)))
            .collect::<String>()
    );
}

fn read_from_file(path: &std::path::PathBuf) -> String {
    read_to_string(path).unwrap_or_else(|_| panic!("could not read file {:?}", path))
}
